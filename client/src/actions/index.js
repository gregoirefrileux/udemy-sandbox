import axios from 'axios';
import { FETCH_USER } from './types';

export const fetchUser = () => async dispatch => {
  // first we execute the ajax request tothe backend server to fetch the user JSON data (if the person is logged in of course, or else it's empty)
  const res = await axios.get('/api/current_user');
  // once the ajax comes back, we dispacth an action to the store with an action type FETCH_USER and the payload sent back by the backend
  dispatch({ type: FETCH_USER, payload: res.data });
};

export const handleToken = token => async dispatch => {
  const res = await axios.post('/api/stripe', token);
  // since we just want to update the user model inside teh state, we can call the same action as before
  dispatch({ type: FETCH_USER, payload: res.data });
};
