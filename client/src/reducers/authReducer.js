import { FETCH_USER } from '../actions/types';

export default function(state = null, action) {
    switch (action.type) {
        case FETCH_USER:
            return action.payload || false; // if the user model is empty, the back returns an empty string. In that case we want to explicitely return false
        default:
            return state;
    }
}