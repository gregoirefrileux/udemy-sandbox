FROM node:8-alpine

# set working directory
RUN mkdir -p /srv/app
WORKDIR /srv/app

# add `/usr/app/node_modules/.bin` to $PATH
# ENV PATH /srv/api/node_modules/.bin:$PATH

# Bundle app source
COPY package.json package-lock.json /srv/app/
# Install app dependencies
RUN npm install

COPY index.js /srv/app/

EXPOSE 5000

# expose volume
VOLUME ["/srv/app"]

# start app
# CMD [ "npm", "run", "start:dev"]
CMD [ "npm", "start"]
