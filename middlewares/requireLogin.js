module.exports = (req, res, next) => {
  // this middleware is used to verify is the user is logged in
  if (!req.user) {
    return res.status(401).send({ error: 'you must log in' });
  }

  next();
};
