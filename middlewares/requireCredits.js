module.exports = (req, res, next) => {
  // this middleware is used to verify is the user is logged in
  if (req.user.credits < 1) {
    return res.status(403).send({ error: "you don't have enough credits" });
  }

  next();
};
