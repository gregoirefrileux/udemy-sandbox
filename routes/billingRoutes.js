const keys = require('../config/keys');
const stripe = require('stripe')(keys.stripeSecretKey);
const requireLogin = require('../middlewares/requireLogin');

module.exports = app => {
  app.post('/api/stripe', requireLogin, async (req, res) => {
    // console.log(req.body);
    // first we send the token to Stripe in order to actually charge the card
    const charge = await stripe.charges.create({
      amount: 500,
      currency: 'usd',
      source: req.body.id,
      description: 'Emaily charge for 5 credits',
    });
    // console.log(charge);

    // now the charge has been successfully created, we add the credit to the user profile
    req.user.credits += 5;
    const user = await req.user.save();

    // now the user has been saved in DB, we can send it back to the front end
    res.send(user);
  });
};
